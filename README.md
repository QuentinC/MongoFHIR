Status
------


Incomplete and experimental.

The branch master is the most advanced and stable. To run the following, you have to clone develop and not master.

The branch validator uses the validator available on HL7 FHIR website. To run it, go to Running Validator


Getting Started On LocalHost
----------------------------


Clone this repository, start MongoDB, and run

	mvn package
	java -Dserver.port=8080 -Dspring.data.mongodb.uri=mongodb://localhost:27017/fhir -jar target/FHIR.jar


This should allow you to run web services with localhost:8080


Configuration For Virtual Machine
---------------------------------


Create a VM and install Java (BEWARE, DO NOT USE OpenJDK), Git and Maven

You can install Maven on CentOS following this tutorial if you are on CentOS : http://tecadmin.net/install-apache-maven-on-centos/
In the source file of Maven, add the following

	JAVA_HOME=(your_java_path)


Then you need to install MongoDB, you can follow this tutorial if you are on CentOS : https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-centos-7
To start the database, run the following

	sudo systemctl start mongod


For now, there is no jar or war. To have the project, you have to clone the repository from Git

Finally, to run the project, go to the repository and run

	mvn clean install
	mvn exec:java



Running Validator
-----------------


To run the validator, clone (or pull) the branch validator.
Then you have to add the validator.jar as a library of the project. To do so, you have to right-click on the jar and click "Add as Library".
Beware of the path for definition ZIP. The file is named validation-min.xml.zip.
You have to change the absolute path in the method create of the ResourceController.