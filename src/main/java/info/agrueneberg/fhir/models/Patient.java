package info.agrueneberg.fhir.models;

import java.util.Date;

/**
 * Created by Quentin on 17/01/2017.
 */
public class Patient {

    private int _id;

    private String _type;

    private int _lid;

    private int _vid;

    private String resourceType;

    private int identifier;

    private boolean active;

    private String name;

    private String gender;

    private Date birthdate;

    private String address;

    private String maritalStatus;

}
