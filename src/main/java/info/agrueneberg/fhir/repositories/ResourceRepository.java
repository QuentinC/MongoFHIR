package info.agrueneberg.fhir.repositories;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import info.agrueneberg.fhir.exceptions.DeletedException;
import info.agrueneberg.fhir.exceptions.IllegalTypeException;
import info.agrueneberg.fhir.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ResourceRepository {

    private final DBCollection resourcesCollection;
    private final DBCollection versionsCollection;

    @Autowired
    private MongoTemplate mongoTemplate;

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    private DBCollection getCollection(String collectionName)
    {
        System.out.println("MongoTemplate"+mongoTemplate);
        return mongoTemplate.getCollection(collectionName);
    }

    @Autowired
    public ResourceRepository(MongoTemplate mongoTemplate) {

        resourcesCollection = mongoTemplate.getCollection("resources");
        versionsCollection = mongoTemplate.getCollection("versions");
    }

    public Map<String, Object> read(String type, int id) throws NotFoundException, DeletedException, IllegalTypeException {

        //Choisir la ressource
        DBCollection table = getCollection(type);

        BasicDBObject query = new BasicDBObject();
        query.put("resourceType", type);
        query.put("identifier", id);
        DBObject doc = table.findOne(query);
        System.out.println(doc);

        /*DBCollection tableA = getCollection("AllergyIntolerance");

        BasicDBObject queryA = new BasicDBObject();
        queryA.put("resourceType", "AllergyIntolerance");
        queryA.put("patient", id);
        DBObject docA = tableA.findOne(queryA);
        System.out.println(docA);*/

        DBObject docF = new BasicDBObject();
        docF.putAll(doc);
        //docF.put("AllergyIntolerance", docA);


        if (doc == null) {

            // Check if the document was deleted
            /*DBObject previousDoc = new BasicDBObject(getPrevious(lid));
            if (previousDoc.containsField("_deleted") && (Boolean) previousDoc.get("_deleted") == true) {
                throw new DeletedException();
            } else {
                throw new NotFoundException();
            }*/

            throw new NotFoundException();
        } else {
            return docF.toMap();
        }

    }

    public Map<String, Object> readComplex(String type, int id, List<String> typeJs) throws NotFoundException, DeletedException, IllegalTypeException {

        //Choisir la ressource
        DBCollection table = getCollection(type);

        //On récupère la première ressource
        BasicDBObject query = new BasicDBObject();
        query.put("resourceType", type);
        query.put("identifier", id);
        DBObject doc = table.findOne(query);
        System.out.println(doc);

        //On prépare un super document avec la première ressource
        DBObject docF = new BasicDBObject();
        docF.putAll(doc);

        if(typeJs == null){
            if (doc == null) {
                throw new NotFoundException();
            } else {
                return doc.toMap();
            }
        }else{
        System.out.println(typeJs);
        for (String typeJ : typeJs) {
            System.out.println(typeJ);

            DBCollection tableA = getCollection(typeJ);

            //On fait une requête en plus par ressources voulues
            //Ne marche que pour Patient en ressources principales en l'état
            //Il faut vérifier les paramètres qui font référence à la ressource
            //Ici c'est patient pour Medication, AllergyIntolerance et subject pour DiagnosticReport
            BasicDBObject queryA = new BasicDBObject();
            queryA.put("resourceType", typeJ);
            if(typeJ.equals("MedicationOrder")) {
                queryA.put("patient", id);
            }else if(typeJ.equals("AllergyIntolerance")){
                    queryA.put("patient", id);
            }else if(typeJ.equals("DiagnosticReport")){
                queryA.put("subject", id);
            }else if(typeJ.equals("Condition")){
                queryA.put("patient", id);
            }
            DBObject docA = tableA.findOne(queryA);
            System.out.println(docA);

            //On rajoute toutes les ressources
            docF.put(typeJ, docA);
        }

            if (doc == null) {
                throw new NotFoundException();
            } else {
                return docF.toMap();
            }
        }
    }

    public Map<String, Object> vread(String type, String lid, Long vid) throws NotFoundException, IllegalTypeException {
        DBObject query = new BasicDBObject();
        query.put("_type", type);
        query.put("_lid", lid);
        query.put("_vid", vid);
        DBObject doc = versionsCollection.findOne(query);
        if (doc == null || (doc.containsField("_deleted") && (Boolean) doc.get("_deleted") == true)) {
            throw new NotFoundException();
        } else {
            doc.removeField("_id");
            return doc.toMap();
        }
    }

    public String create(String type, Map<String, Object> entity) throws IllegalTypeException {

        //Choisir la ressource
        DBCollection table = getCollection(type);

        DBObject doc = new BasicDBObject(entity);
        /*doc.removeField("_id");
        doc.removeField("_deleted");*/
        String lid = generateUUID();
        //doc.put("_vid", new Long(1));
        table.insert(doc);
        //versionsCollection.insert(doc);
        return lid;
    }

    public void update(String type, int id, Map<String, Object> entity) throws IllegalTypeException {

        DBCollection table = getCollection(type);

        DBObject doc = new BasicDBObject(entity).append("set", entity);

        /*doc.removeField("_id");
        doc.removeField("_deleted");*/
        /*doc.put("resourceType", type);
        doc.put("identifier", id);*/

        // Find previous doc
        DBObject query = new BasicDBObject();
        query.put("resourceType", type);
        query.put("identifier", id);
        DBObject previousDoc = table.findOne(query);
//        if (previousDoc != null) {
//            table.remove(previousDoc);
//        }
        table.update(previousDoc, doc);
        //versionsCollection.insert(doc);
    }

    public void delete(String type, int id) throws NotFoundException, IllegalTypeException {

        DBCollection table = getCollection(type);

        DBObject query = new BasicDBObject();
        query.put("resourceType", type);
        query.put("identifier", id);
        DBObject previousDoc = table.findOne(query);
        if (previousDoc == null) {
            throw new NotFoundException();
        } else {
//            DBObject doc = new BasicDBObject();
//            doc.put("_type", type);
//            doc.put("_lid", id);
//            doc.put("_vid", ((Number) previousDoc.get("_vid")).longValue() + 1);
//            doc.put("_deleted", true);
            table.remove(previousDoc);
            //versionsCollection.insert(doc);
        }
    }

    public List<Map<String, Object>> search(String type, Map<String, String[]> parameters) {

        //Choisir la ressource
        DBCollection table = getCollection(type);

        DBObject query = new BasicDBObject();
        if (type != null) {
            query.put("_type", type);
        }
        for (Map.Entry<String, String[]> entry : parameters.entrySet()) {
            query.put(entry.getKey(), entry.getValue()[0]);
        }
        DBCursor cursor = table.find(query);
        List<Map<String, Object>> docs = new ArrayList<Map<String, Object>>(cursor.count());
        try {
            while(cursor.hasNext()) {
                DBObject doc = cursor.next();
                doc.removeField("_id");
                docs.add(doc.toMap());
            }
        } finally {
            cursor.close();
        }
        return docs;
    }

    public List<Map<String, Object>> history() {
        DBObject query = new BasicDBObject();
        query.put("_deleted", new BasicDBObject("$exists", false));
        DBObject orderBy = new BasicDBObject();
        orderBy.put("_type", 1);
        orderBy.put("_lid", 1);
        orderBy.put("_vid", 1);
        DBCursor cursor = versionsCollection.find(query).sort(orderBy);
        List<Map<String, Object>> docs = new ArrayList<Map<String, Object>>(cursor.count());
        try {
            while(cursor.hasNext()) {
                DBObject doc = cursor.next();
                doc.removeField("_id");
                docs.add(doc.toMap());
            }
        } finally {
            cursor.close();
        }
        return docs;
    }

    public List<Map<String, Object>> history(String type) {
        DBObject query = new BasicDBObject();
        query.put("_type", type);
        query.put("_deleted", new BasicDBObject("$exists", false));
        DBObject orderBy = new BasicDBObject();
        orderBy.put("_lid", 1);
        orderBy.put("_vid", 1);
        DBCursor cursor = versionsCollection.find(query).sort(orderBy);
        List<Map<String, Object>> docs = new ArrayList<Map<String, Object>>(cursor.count());
        try {
            while(cursor.hasNext()) {
                DBObject doc = cursor.next();
                doc.removeField("_id");
                docs.add(doc.toMap());
            }
        } finally {
            cursor.close();
        }
        return docs;
    }

    public List<Map<String, Object>> history(String type, String lid) {
        DBObject query = new BasicDBObject();
        query.put("_type", type);
        query.put("_lid", lid);
        query.put("_deleted", new BasicDBObject("$exists", false));
        DBObject orderBy = new BasicDBObject();
        orderBy.put("_vid", 1);
        DBCursor cursor = versionsCollection.find(query).sort(orderBy);
        List<Map<String, Object>> docs = new ArrayList<Map<String, Object>>(cursor.count());
        try {
            while(cursor.hasNext()) {
                DBObject doc = cursor.next();
                doc.removeField("_id");
                docs.add(doc.toMap());
            }
        } finally {
            cursor.close();
        }
        return docs;
    }

    private Map<String, Object> getPrevious(String lid) throws NotFoundException {
        DBObject query = new BasicDBObject();
        query.put("_lid", lid);
        DBObject orderBy = new BasicDBObject();
        orderBy.put("_vid", -1);
        DBObject doc = versionsCollection.findOne(query, null, orderBy);
        if (doc == null) {
            throw new NotFoundException();
        } else {
            return doc.toMap();
        }
    }

    public static String generateUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}