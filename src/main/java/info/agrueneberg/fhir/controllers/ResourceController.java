package info.agrueneberg.fhir.controllers;

import info.agrueneberg.fhir.exceptions.AccessDeniedException;
import info.agrueneberg.fhir.exceptions.DeletedException;
import info.agrueneberg.fhir.exceptions.IllegalTypeException;
import info.agrueneberg.fhir.exceptions.NotFoundException;
import info.agrueneberg.fhir.services.MetadataService;
import info.agrueneberg.fhir.services.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import org.springframework.security.core.context.SecurityContextHolder;

@Controller
@RequestMapping(produces = "application/json")
@ResponseBody
public class ResourceController {

    private final ResourceService resourceService;
    private final MetadataService metadataService;

    @Autowired
    public ResourceController(ResourceService resourceService, MetadataService metadataService) {
        this.resourceService = resourceService;
        this.metadataService = metadataService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> search(HttpServletRequest request) throws AccessDeniedException {
       /* User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean hasAccess = metadataService.hasPermission(request.getServletPath(), user, "list");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }*/
        Map<String, String[]> parameters = request.getParameterMap();
        List<Map<String, Object>> resources = resourceService.search(parameters);
        Iterator<Map<String, Object>> itr = resources.iterator();
        while (itr.hasNext()) {
            Map<String, Object> embeddedResource = itr.next();
            String embeddedResourcePath = "/" + embeddedResource.get("_type") + "/" + embeddedResource.get("_lid");
            /*boolean includeEmbeddedResource = metadataService.hasPermission(embeddedResourcePath, user, "read");
            if (!includeEmbeddedResource) {
                itr.remove();
            }*/
        }
        return resources;
    }

    @RequestMapping(value = "/_history", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> history() throws AccessDeniedException {
        //User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String resourcePath = "/";
        /*boolean hasAccess = metadataService.hasPermission(resourcePath, user, "history");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }*/
        List<Map<String, Object>> resources = resourceService.history();
        Iterator<Map<String, Object>> itr = resources.iterator();
        while (itr.hasNext()) {
            Map<String, Object> embeddedResource = itr.next();
            String embeddedResourcePath = "/" + embeddedResource.get("_type") + "/" + embeddedResource.get("_lid");
            /*boolean includeEmbeddedResource = metadataService.hasPermission(embeddedResourcePath, user, "read");
            if (!includeEmbeddedResource) {
                itr.remove();
            }*/
        }
        return resources;
    }

    @RequestMapping(value = "/{type}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> searchForTypeImplicit(@PathVariable String type, HttpServletRequest request) throws AccessDeniedException {
        /*User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean hasAccess = metadataService.hasPermission(request.getServletPath(), user, "list");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }*/
        Map<String, String[]> parameters = request.getParameterMap();
        List<Map<String, Object>> resources = resourceService.search(type, parameters);
        Iterator<Map<String, Object>> itr = resources.iterator();
        while (itr.hasNext()) {
            Map<String, Object> embeddedResource = itr.next();
            String embeddedResourcePath = "/" + embeddedResource.get("_type") + "/" + embeddedResource.get("_lid");
            /*boolean includeEmbeddedResource = metadataService.hasPermission(embeddedResourcePath, user, "read");
            if (!includeEmbeddedResource) {
                itr.remove();
            }*/
        }
        return resources;
    }

    @RequestMapping(value = "/{type}", method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@PathVariable String type, @RequestBody Map<String, Object> entity, HttpServletRequest request,
                       HttpServletResponse response) throws IllegalTypeException, AccessDeniedException {

        String lid = resourceService.create(type, entity);
    }

    @RequestMapping(value = "/{type}/_search", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> searchForTypeExplicit(@PathVariable String type, HttpServletRequest request) throws AccessDeniedException {
        //User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String resourcePath = "/" + type;
        /*boolean hasAccess = metadataService.hasPermission(resourcePath, user, "list");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }*/
        Map<String, String[]> parameters = request.getParameterMap();
        List<Map<String, Object>> resources = resourceService.search(type, parameters);
        Iterator<Map<String, Object>> itr = resources.iterator();
        while (itr.hasNext()) {
            Map<String, Object> embeddedResource = itr.next();
            String embeddedResourcePath = "/" + embeddedResource.get("_type") + "/" + embeddedResource.get("_lid");
            /*boolean includeEmbeddedResource = metadataService.hasPermission(embeddedResourcePath, user, "read");
            if (!includeEmbeddedResource) {
                itr.remove();
            }*/
        }
        return resources;
    }

    @RequestMapping(value = "/{type}/_history", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> historyForType(@PathVariable String type) throws AccessDeniedException {
        //User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String resourcePath = "/" + type;
        /*boolean hasAccess = metadataService.hasPermission(resourcePath, user, "history");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }*/
        List<Map<String, Object>> resources = resourceService.history(type);
        Iterator<Map<String, Object>> itr = resources.iterator();
        while (itr.hasNext()) {
            Map<String, Object> embeddedResource = itr.next();
            String embeddedResourcePath = "/" + embeddedResource.get("_type") + "/" + embeddedResource.get("_lid");
            /*boolean includeEmbeddedResource = metadataService.hasPermission(embeddedResourcePath, user, "read");
            if (!includeEmbeddedResource) {
                itr.remove();
            }*/
        }
        return resources;
    }

    @RequestMapping(value = "/simple/{type}/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> read(@PathVariable String type, @PathVariable int id, HttpServletRequest request)
            throws NotFoundException, DeletedException, IllegalTypeException, AccessDeniedException {

        return resourceService.read(type, id);
    }

    @RequestMapping(value = "/complex/{type}/{id}", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> readComplex(@PathVariable String type, @PathVariable int id,
                                           @RequestBody List<String> typeJs, HttpServletRequest request)
            throws NotFoundException, DeletedException, IllegalTypeException, AccessDeniedException {

        return resourceService.readComplex(type, id, typeJs);
    }

   @RequestMapping(value = "/{type}/{id}", method = RequestMethod.PUT, consumes = "application/json")
   @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable String type, @PathVariable int id, @RequestBody Map<String, Object> entity, HttpServletRequest request, HttpServletResponse response) throws IllegalTypeException, AccessDeniedException {
        /*User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean hasAccess = metadataService.hasPermission(request.getServletPath(), user, "write");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }*/
        resourceService.update(type, id, entity);
        //response.addHeader("Location", "/" + type + "/" + id + "/_history/" + vid);
        /*if (vid == 1) {
            response.setStatus(201);
        } else {
            response.setStatus(200);
        }*/
    }

    @RequestMapping(value = "/{type}/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String type, @PathVariable int id, HttpServletRequest request) throws NotFoundException, IllegalTypeException, AccessDeniedException {
        /*User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean hasAccess = metadataService.hasPermission(request.getServletPath(), user, "write");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }*/
        resourceService.delete(type, id);
    }

    @RequestMapping(value = "/{type}/{lid}/_history", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> historyForResource(@PathVariable String type, @PathVariable String lid) throws AccessDeniedException {
        //User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String resourcePath = "/" + type + "/" + lid;
        /*boolean hasAccess = metadataService.hasPermission(resourcePath, user, "history");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }*/
        List<Map<String, Object>> resources = resourceService.history(type, lid);
        Iterator<Map<String, Object>> itr = resources.iterator();
        while (itr.hasNext()) {
            Map<String, Object> embeddedResource = itr.next();
            String embeddedResourcePath = "/" + embeddedResource.get("_type") + "/" + embeddedResource.get("_lid");
            //boolean includeEmbeddedResource = metadataService.hasPermission(embeddedResourcePath, user, "read");
            /*if (!includeEmbeddedResource) {
                itr.remove();
            }*/
        }
        return resources;
    }

    /*@RequestMapping(value = "/{type}/{lid}/_history/{vid}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> vread(@PathVariable String type, @PathVariable String lid, @PathVariable Long vid) throws NotFoundException, IllegalTypeException, AccessDeniedException {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String resourcePath = "/" + type + "/" + lid;
        boolean hasAccess = metadataService.hasPermission(resourcePath, user, "history");
        if (!hasAccess) {
            throw new AccessDeniedException();
        }
        return resourceService.vread(type, lid, vid);
    }

    @RequestMapping(value = "/{type}/_validate", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void validate(@PathVariable String type) {
    }*/

}